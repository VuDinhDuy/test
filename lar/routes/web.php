<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',['as'=>'dangNhap',function()
{
	return view('welcome1');
}]);
Route::get('trangchu', ['as'=>'trangchu', function()
{
	return view('welcome');
}]);
Route::get('taodata',['as'=>'taodata', function()
{
	Schema::create('thong_tin', function ($table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('phone_number');
            $table->string('email');
            $table->timestamps();
        });
	Schema::create('login', function ($table) {
            $table->bigIncrements('id');
            $table->string('username');
            $table->string('password');
            $table->timestamps();
        });
}]);
Route::post('/postDangNhap',['as'=>'postDangNhap','uses'=>'Controller@postDangNhap']);

Route::post('postMakeAccout',['as'=>'postMakeAccout','uses'=>'Controller@postMakeAccout']);

Route::post('controller/postAddAccout',['as'=>'postAddAccout','uses'=>'Controller@postAddAccout']);

Route::post('controller/postDelete',['as'=>'postDelete','uses'=>'Controller@postDelete']);

Route::post('controller/postUpdate',['as'=>'postUpdate','uses'=>'Controller@postUpdate']);

Route::post('controller/postSelect',['as'=>'postSelect','uses'=>'Controller@postSelect']);

Route::post('controller/getSelect/postSelectName',['as'=>'postSelectName','uses'=>'Controller@postSelectName']);

Route::post('controller/getSelect/postSelectPhone',['as'=>'postSelectPhone','uses'=>'Controller@postSelectPhone']);

Route::post('controller/getSelect/postSelectEmail',['as'=>'postSelectEmail','uses'=>'Controller@postSelectEmail']);

Route::post('controller/viewall', ['as'=>'viewall','uses'=>'Controller@viewall']);