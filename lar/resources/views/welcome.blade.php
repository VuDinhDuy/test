<p align="right"><a href="{!!route('dangNhap')!!}">Thoát</a></p>
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="resources/js/vue.js"></script>

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Kho Dữ Liệu
                </div>

                <div id="app" class="links">
                    <form method="POST" action="{!!route('viewall')!!}">
                        <input type="hidden" name="_token" value="{!! csrf_token()!!}"><br>
                        <input type="submit" name="post_click" value="All Information">
                </form>
                    <button v-on:click="add=!add">Add New Infor</button>
                    <button v-on:click="update=!update">Update Infor</button>
                    <button v-on:click="del=!del">Delete Infor</button>
                    <button v-on:click="search=!search">Search Infor</button>
                    <template v-if="add" class="content">
                        <hr>
                        <form method="POST" action="{!!route('postAddAccout')!!}">
                            <input type="hidden" name="_token" value="{!! csrf_token()!!}">
                            <table>
                                <tr>
                                    <td width="30%">Name: </td>
                                    <td><input type="text" name="name" value=""/></td>
                                </tr>
                                <tr>
                                    <td width="30%">Phone: </td>
                                    <td><input type="text" name="phone" value=""/></td>
                                </tr>
                                <tr>
                                    <td width="30%">Email: </td>
                                    <td><input type="text" name="email" value=""/></td>
                                </tr>
                                <tr>
                                    <td rowspan="2"><input type="submit" name="post_click" value="Them Moi"></td>
                                </tr>
                            </table>
                        </form>
                    </template>

                    <template v-if="update" class="content">
                        <hr>
                        <form method="POST" action="{!!route('postUpdate')!!}">
                            <input type="hidden" name="_token" value="{!! csrf_token()!!}">
                            <table>
                                <tr>
                                    <td width="40%">Name: </td>
                                    <td><input type="text" name="name" value=""/></td>
                                </tr>
                                <tr>
                                    <td width="40%">Phone: </td>
                                    <td><input type="text" name="phone" value=""/></td>
                                </tr>
                                <tr>
                                    <td width="40%">Email: </td>
                                    <td><input type="text" name="email" value=""/></td>
                                </tr>
                                 <tr>
                                    <td width="40%">Change Name: </td>
                                    <td><input type="text" name="name" value=""/></td>
                                </tr>
                                <tr>
                                    <td width="40%">Change Phone: </td>
                                    <td><input type="text" name="phone" value=""/></td>
                                </tr>
                                <tr>
                                    <td width="40%">Change Email: </td>
                                    <td><input type="text" name="email" value=""/></td>
                                </tr>

                                <tr>
                                    <td rowspan="2"><input type="submit" name="post_click" value="Thay doi"></td>
                                </tr>
                            </table>
                        </form>
                    </template>

                    <template v-if="del" class="content">
                        <hr>
                        <form method="POST" action="{!!route('postDelete')!!}">
                            <input type="hidden" name="_token" value="{!! csrf_token()!!}">
                            <table>
                                <tr>
                                    <td width="30%">Name: </td>
                                    <td><input type="text" name="name" value=""/></td>
                                </tr>
                                <tr>
                                    <td width="30%">Phone: </td>
                                    <td><input type="text" name="phone" value=""/></td>
                                </tr>
                                <tr>
                                    <td width="30%">Email: </td>
                                    <td><input type="text" name="email" value=""/></td>
                                </tr>
                                <tr>
                                    <td rowspan="2"><input type="submit" name="post_click" value="Them Moi"></td>
                                </tr>
                            </table>
                        </form>
                    </template>

                    <template v-if="search" class="content">
                        <hr>
                      <button v-on:click="name=!name">Tim Theo Ten</button>
                      <button v-on:click="phone!=phone">Tim Theo So Dien Thoai</button>
                      <button v-on:click="email!=email">Tim Theo Email</button>
                      <hr>
                      <template v-if="name">
                        <hr>
                          <form method="POST" action="{!!route('postSelectName')!!}">
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                            Name: <input type="text" name="name" value=""><br>
                            <input type="hidden" name="phone" value="null"><br>
                            <input type="hidden" name="email" value="null"><br>
                            <input type="submit" name="post_click" value="Tim Kiem">
                      </template>
                      <template v-if="phone">
                        <hr>
                          <form method="POST" action="{!!route('postSelectPhone')!!}">
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                            Phone: <input type="text" name="phone" value=""><br>
                            <input type="hidden" name="name" value="null"><br>
                            <input type="hidden" name="email" value="null"><br>
                            <input type="submit" name="post_click" value="Tim Kiem">
                      </template>
                      <template v-if="email">
                        <hr>
                          <form method="POST" action="{!!route('postSelectEmail')!!}">
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                            Email: <input type="text" name="email" value=""><br>
                            <input type="hidden" name="phone" value="null"><br>
                            <input type="hidden" name="name" value="null"><br>
                            <input type="submit" name="post_click" value="Tim Kiem">
                      </template>
                        </form>
                    </template>
                </div>
            </div>
        </div>
        <script>
            var app = new Vue({
                el: '#app',
                data: {
                    add: false,
                    update: false,
                    del: false,
                    search: false,
                    name: false,
                    phone: false,
                    email: false,
                },
            });
        </script>
    </body>
</html>
