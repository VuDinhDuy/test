<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Response;
class marcoServiceProvide extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        Response::macro('cap', function($str)
        {
            return Response::make(strtoupper($str));
        });
        Response::macro('input', function($contact)
        {
            $contact= '
            <form  method="POST">
           Ho ten <input type="text" /><br />
           So dien thoai <input type="text"/><br /> 
           </form>';
           return $contact;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
