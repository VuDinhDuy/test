<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation;
use App\Http\Requests\vuduy;
use App\Http\Requests\login;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Http\Controllers;
use Database\Seeds\DatabaseSeeder;
use App\vuduy_model;
use Illuminate\Support\Facades\DB;
use App\login_model;
use Resources\js\vue;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs;
    public function postAddAccout(vuduy $request)
    {
    	$name= $request->name;
    	$phone= $request->phone;
    	$email= $request->email;
    	DB::table('thong_tin')->insert(['name'=> $name,'phone_number'=>$phone,'email'=>$email]);
        echo "Them Thanh Cong"."<br>";
        return view('Home');
    }

    public function postDelete(vuduy $request)
    {
    	$name=$request->name;
    	$phone=$request->phone;
    	$email=$request->email;
    	$check=DB::table('thong_tin')->where('name',$name)->where('phone_number',$phone)->where('email',$email)->get();
    	if($check=="[]")
    	{
    		echo "Khong co doi tuong nao trong danh sach co thong tin tren"."<br>";
    		return view('Home');
    	}
    	else
    	{
    	DB::table('thong_tin')->where('name',$name)->where('phone_number',$phone)->where('email',$email)->delete();
    	echo "Xoa thanh cong"."<br>";
    	return view('Home');
    	}
    }
  
    public function postUpdate(vuduy $request)
    {
    	$name=$request->name;
    	$phone=$request->phone;
    	$email=$request->email;
    	$namerep=$request->namerep;
    	$phonerep=$request->phonerep;
    	$emailrep=$request->emailrep;
    	$check=DB::table('thong_tin')->where('name',$name)->where('phone_number',$phone)->where('email',$email)->get();
    	$checkchance=DB::table('thong_tin')->where('name',$namerep)->where('phone_number',$phonerep)->where('email',$emailrep)->get();
    	if($check=="[]")
    	{
    		echo "Khong co doi tuong nao trong danh sach co thong tin tren"."<br>";
    		return view('Home');
    	}
    	else
    	{
    		if($checkchance=="[]")
    		{
    			echo "Khong co thong tin can thay doi"."<br>";
    			return view('Home');
    		}
    		else
    		{
       	DB::table('thong_tin')->where('name',$name)->where('phone_number',$phone)->where('email',$email)->update(['name'=>$namerep,'phone_number'=>$phonerep,'email'=>$emailrep]);
    	echo "Thay doi thanh cong"."<br>";
    	return view('Home');
    	}
    }
    }

    public function postSelectName(vuduy $request)
    {
    	$name=$request->name;
    	$check=DB::table('thong_tin')->where('name',$name)->get();
    	echo "<table border align='center' width=100%>
    				<tr>
    				<th width=10%>ID</th>
    				<th width=30%>NAME</th>
    				<th width=30%>PHONE_NUMBER</th>
    				<th width=30%>EMAIL</th>
    				</tr>
    				</table>";
    	if($check=="[]")
    	{
    		echo "Khong co doi tuong nao co thong tin nhu tren"."<br>";
    		return view('Home');
    	}
    	else
    	{
    		$max=DB::table('thong_tin')->max('id');
    		$i=1;
    		while($i<=$max)
    		{
    			$test=DB::table('thong_tin')->where('id',$i)->value('name');
    			if($test!=$name)
    			{
    				$i++;
    			}
    			else
    			{
    				$checkid=DB::table('thong_tin')->where('id',$i)->where('name', $name)->value('id');
    				$checkname=DB::table('thong_tin')->where('id',$i)->where('name', $name)->value('name');
    				$checkphone=DB::table('thong_tin')->where('id',$i)->where('name', $name)->value('phone_number');
    				$checkemail=DB::table('thong_tin')->where('id',$i)->where('name',$name)->value('email');
    				echo "<table border align='center' width=100%>
    				<tr>
    				<td width=10%><p align=center>$checkid</p></td>
    				<td width=30%><p align=center>$checkname</p></td>
    				<td width=30%><p align=center>$checkphone</p></td>
    				<td width=30%><p align=center>$checkemail</p></td>
    				</tr>
    				</table>";
    				$i++;
    			}
    		}
    		return view('Home');
    	}
    }

    public function postSelectEmail(vuduy $request)
    {
    	$email=$request->email;
    	$check=DB::table('thong_tin')->where('email',$email)->get();
    	echo "<table border align='center' width=100%>
    				<tr>
    				<th width=10%>ID</th>
    				<th width=30%>NAME</th>
    				<th width=30%>PHONE_NUMBER</th>
    				<th width=30%>EMAIL</th>
    				</tr>
    				</table>";
    	if($check=="[]")
    	{
    		echo "Khong co doi tuong nao co thong tin nhu tren"."<br>";
    		return view('Home');
    	}
    	else
    	{
    		$max=DB::table('thong_tin')->max('id');
    		$i=1;
    		while($i<=$max)
    		{
    			$test=DB::table('thong_tin')->where('id',$i)->value('email');
    			if($test!=$email)
    			{
    				$i++;
    			}
    			else
    			{
    				$checkid=DB::table('thong_tin')->where('id',$i)->where('email', $email)->value('id');
    				$checkname=DB::table('thong_tin')->where('id',$i)->where('email', $email)->value('name');
    				$checkphone=DB::table('thong_tin')->where('id',$i)->where('email', $email)->value('phone_number');
    				$checkemail=DB::table('thong_tin')->where('id',$i)->where('email',$email)->value('email');
    				echo "<table border align='center' width=100%>
    				<tr>
    				<td width=10%><p align=center>$checkid</p></td>
    				<td width=30%><p align=center>$checkname</p></td>
    				<td width=30%><p align=center>$checkphone</p></td>
    				<td width=30%><p align=center>$checkemail</p></td>
    				</tr>
    				</table>";
    				$i++;
    			}
    		}
    		return view('Home');
    	}
    }

    public function postSelectPhone(vuduy $request)
    {
    	$phone=$request->phone;
    	$check=DB::table('thong_tin')->where('phone_number',$phone)->get();
    	echo "<table border align='center' width=100%>
    				<tr>
    				<th width=10%>ID</th>
    				<th width=30%>NAME</th>
    				<th width=30%>PHONE_NUMBER</th>
    				<th width=30%>EMAIL</th>
    				</tr>
    				</table>";
    	if($check=="[]")
    	{
    		echo "Khong co doi tuong nao co thong tin nhu tren"."<br>";
    		return view('Home');
    	}
    	else
    	{
    		$max=DB::table('thong_tin')->max('id');
    		$i=1;
    		while($i<=$max)
    		{
    			$test=DB::table('thong_tin')->where('id',$i)->value('phone_number');
    			if($test!=$phone)
    			{
    				$i++;
    			}
    			else
    			{
    				$checkid=DB::table('thong_tin')->where('id',$i)->where('phone_number', $phone)->value('id');
    				$checkname=DB::table('thong_tin')->where('id',$i)->where('phone_number', $phone)->value('name');
    				$checkphone=DB::table('thong_tin')->where('id',$i)->where('phone_number', $phone)->value('phone_number');
    				$checkemail=DB::table('thong_tin')->where('id',$i)->where('phone_number',$phone)->value('email');
    				echo "<table border align='center' width=100%>
    				<tr>
    				<td width=10%><p align=center>$checkid</p></td>
    				<td width=30%><p align=center>$checkname</p></td>
    				<td width=30%><p align=center>$checkphone</p></td>
    				<td width=30%><p align=center>$checkemail</p></td>
    				</tr>
    				</table>";
    				$i++;
    			}
    		}
    		return view('Home');
    	}
    }
    public function viewall()
    {
    	return view('view_all');
    }
    public function postSelect(vuduy $request)
    {
    	$name=$request->name;
    	$phone=$request->phone;
    	$email=$request->email;
    	if(!empty($name))
    	echo $name;
    	if(!empty($phone))
    	echo $phone;
    	if(empty($email))
    	echo $email;
    }
    public function postDangNhap(login $request)
    {
        $username=$request->username;
        $password=$request->password;
        $check=DB::table('login')->where('username',$username)->where('password',$password)->get();
        if($check!="[]")
        {
            return view('welcome');
        }
        else
        {
            echo "Tai khoan hoac mat khau khong dung"."<br>";
            return view('welcome1');
        }
    }
    public function postMakeAccout(login $request)
    {
        $username=$request->username;
        $password=$request->password;
        $check=DB::table('login')->where('username',$username)->get();
        if($check=="[]")
        {
            DB::table('login')->insert(['username'=>$username,'password'=>$password]);
            echo "Tao Thanh Cong";
            return view('welcome');
        }
        else
        {
            echo "Tai khoan da ton tai";
            return view('welcome1');
        }
    }
}
