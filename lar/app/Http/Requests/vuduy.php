<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class vuduy extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'phone'=>'required',
            'email'=>'required'
        ];
    }
    public function messages()
    {
        return [
            'name.required'=>'vui long nhap ten',
            'phone.required'=>'vui long nhap phone number',
            'email.required'=>'vui long nhap email'];
    }
}
